---
title: October 2017 meetup
date: 2017-10-14 10:10:01 +0530
desc: October 2017 meetup at Don Bosco Institute of Technology.
category: event
---

The October 2017 meeting of "Indian Linux User Group, Mumbai (ILUG-BOM)",
will be held on 14th October 2017, 15:00 to 16:00 IST.

**Venue**:  

IT Lab 4, Third Floor, A - Wing,  
Department of Information Technology,  
Don Bosco Institute of Technology, Kurla (W).  
10 min from Vidyavihar Station (W).  
<a href="https://goo.gl/maps/fRQoetu97Vp" target="_blank">View Map</a>

**Agenda:** 
1. 30 minutes: Introductions
2. 1.5 hour: Introducing FOSS to teachers
    1. Installing GNU/Linux
    2. Basic usage
    3. Common day to day tools (Web browsing, documents, spreadsheets,
         presentations)

**Meeting Summary**

<div align="justify">

--Excerpts of the meeting by Prahlad C

After transforming Auxilium School wadala to Linux, ILUG BOM had an
excellent meeting with teachers and students on 14th Oct 2017 at
DBIT-Kurla.

Though good number of  teachers have registered for the meet but due to
heavy rains many haven't dared to take risk and some due to personal
reasons. No problem, there is always a second time.

The meet started with the introduction of everyone including the new
comers. few were completely new to Linux. They showed lot of interest in
learning Linux.

Mr. Joe Steve c ms and softwares, in short, why and what of Linux. He
highlighted the need for FOSS and how it should be, in the reach of common
man as it is community driven. Replying to Shashwat's (a student of 12th
standard) question, the difference between hackers and crackers Joe
narrated an interesting story on hackers and also mentioned why that word
was coined.

Mr. Rajeev unpacked his bundle of knowledge and explained the Linux Fedora
starting from installation of the OS which is customized for schools and
school teachers. He dealt with most of the applications which are useful to
teachers such as tuxpaint, tuxmath, Unicode for devanagari script etc. Most
interestingly he made an impeccable explanation of Libre office giving
importance to Libre Writer, Libre calc and Libre impress which are far
better than their counterpart softwares like Office word, Excel sheet and
Power Point Presentation of MS Office. Mr. Rajeev cleared the doubts of
teachers in a lucid language. At the end of his session Mr. Rajeev gifted
everyone with a bootable DVD of Fedora.

Next, Mr. Raghavendra Kamath a professional artist, perspicuously (with
clarity and lucidity) explained GIMP (GNU Image Manipulation Program) to
everyone's understanding. He emphasized on the tools that are available in
GIMP starting from types of brushes of different thickness,  different
colors, different pallets etc. He demonstrated multilingual text usage in
GIMP especially combination of two letters in Marathi or Hindi, to be
precise Devanagari script. He beautifully showed with ease that how one can
touch, cut, crop and change the complexion of the an image and also warned,
public of it's misuse.

Mrs. Sarita Mulay a representative of Shishuvan School of Matunga offered
us a pleasant chance to meet the teachers of her School and demonstrate
Linux starting from installation, to use of all applications which are
beneficial to teachers but only when school reopens after Diwali Vacation.
She also assured us of place in her School for our future meet ups.

At the end all dispersed with a sense of gratitude and achieving some thing
new. All of them (new entrants) showed interest in continuing with us in
the journey of Linux and also to help us in our mission.

</div>

[View original post on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20171009/075092.html).  
[View original Meeting update on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20171016/075094.html).  
