---
title: December 2017 meetup
date: 2017-12-06 10:10:01 +0530
desc: October 2017 meetup at Frappé Technologies Pvt. Ltd.
category: event
---

The December 2017 meeting of "Indian Linux User Group, Mumbai (ILUG-
BOM)", was held on 9th December 2017, 15:00 to 16:30 IST.

**Venue**:  
Frappe Technologies Private Limited,
D/324 Neelkanth Business Park,
Vidyavihar West, Mumbai, Maharashtra - 400086, India  
<a href="https://www.openstreetmap.org/node/5268038382" target="_blank">View Map</a>

**Agenda:**
   1. 30 minutes: Introductions
   2. 1 Hour: "How to do charting from scratch in 2017" by Prateeksha Singh, Frappe Technologies Private Limited.
   3. Floor open for relevant discussions

Everyone is welcome :)

**Meeting Summary**

<div align="justify">
Meeting Summary will be updated soon.
</div>

[View original post on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20171204/075109.html).  
