---
title: September 2017 meetup
date: 2017-09-04 14:16:01 +0530
desc: September 2017 meetup at Don Bosco Institute of Technology.
category: event
---

The September 2017 meeting of "Indian Linux User Group, Mumbai (ILUG-BOM)",
was held on 09th September 2017, 15:00 to 16:00 IST.

**Venue**:  

IT Lab 4, Third Floor, A - Wing,  
Department of Information Technology,  
Don Bosco Institute of Technology, Kurla (W).  
10 min from Vidyavihar Station (W).  
<a href="https://goo.gl/maps/fRQoetu97Vp" target="_blank">View Map</a>

**Agenda:** 
1. 15 minutes: Introductions
2. 1 hour: "How I came to write a full-fledged OSS product with RDD(README-Driven Development)" - Achilles Rasquinha
3. Floor open for relevant discussions.

**Meeting Summary**

<div align="justify">

Meeting Summary by R. K. Rajeev

Hi Everyone,

The meeting on the 9th of September 2017 started off by 3 pm, with a small
group of regulars(Including milind and raghu) and a few students from DBIT
coming together for a round of introductions. We also got a mini intro from
our speaker for the day, Achilles. We waited a short while as the logistics
of projection and access to the relevant github accounts were being sorted,
allowing a few more participants to troop into the room. We then started on
a fascinating look into a FOSS developer's mindset, and watched how
software would evolve through the prism of the various projects that
Achilles has been involved with. We started off with Achilles professing
his deep love for python and clean code, and graduated to the importance of
simple and neat API's, where he used the versatile python Request library
as an example of how to build a powerful but still clean API interface. We
then learnt how Readme Driven Development(RDD) is one of the various
paradigms that can be used to drive development of a FOSS project, and that
it is ideally suited to managing small to mid sized projects, generally
centred around one primary author + maintainer. The session was peppered
with a lot of interesting detours into a variety of topics, ranging from a
discussion of the importance of open data sets, and the state of software
in the bioinformatics domain. The participants then got involved, with
Achilles throwing the floor open to all types of questions, especially from
the DBIT student participants. In fact, with the lively Q&A session that
followed Achilles's talk, Milind had to step in and remind both the speaker
and the audience that we had run out of time.  

The action then shifted to the 2 guest foundations that were present at the
meeting. First up was the Digital Freedom Foundation(DFF), represented by
its founder, Mr Krishnakant Mane, also an ilug-bom old timer whom we were
glad welcome back after an extended break. His talk, covering a range of
topics, centred on the importance of FOSS in education, also drew a lot of
parallels with our recently concluded outreach activity at St. Mary's, and
highlighted a number of avenues where the DFF and ILUG-BOM could
collaborate in this sphere. Next, we had representatives of the Free
Software Movement of Maharashtra(FSMM) step up and take the stage, drawing
the audience's attention towards the more intangible aspects of "Freedom"
as it applies in a digital world. Using a variety of examples like
Facebook's thankfully-failed FreeBasics Initiative, they explained how the
importance of Freedom reaches far beyond just the ability to study and
modify the code of the software that we may run(or in some cases, likely
runs us). They also touched on the rather depressing statistics showing
that India's high rate of female engineering enrolment does not translate
to a corresponding rate of involvement in the IT world, with barely 30% of
the internet user-base being women. Such issues are deep, social questions
with a lot to be said on all sides, and while they aren't necessarily
technical issues, but technology and especially FOSS can play a part in
evolving solutions for them. KK from DFF then threw in an interesting
counter-example, where almost 90% of the contributors to GNUKhatha, the
financial accounting system being developed as a tally alternative, were
women.  

The meeting ended with the audience breaking up to partake in refreshments,
but the discussions did not. The tea session saw lively discussion on the
various topics raised during the meeting, and also a lot of planning on how
to take some of the ideas forward, especially with identifying avenues for
collaboration between ILUG-BOM, DFF and FSMM. We even had a interesting
side discussion on the importance of software documentation, with Achilles
advocating a moderate approach to encouraging developers to write
documentation and KK advocating that developers who didn't write docs
should get their pay docked ;-D  

All in all, a most engaging meeting, full of knowledge exchange and
transfer of ideas, and loads of audience interaction. This summary, by its
very nature has to leave out a lot of what happened, but i hope that i have
touched on the most important and interesting bits of the meet. As always,
please feel free to add in any bit that i may have missed and that you feel
is important and needs to be placed in the record.  

P.S. : I believe that after i left, there was another round-chair
discussion with a focus on the activities leading up to the Software
Freedom Day, and since i wasn't there, i am pasting below, verbatim,
Milind's excellent synopsis of the post-meet.  

-- Milind's Post  
After the meeting we had an informal discussion about how ILUG-BOM,DFF,FSMM
and DBIT can join hands and make free software movement even stronger. It
was decided to meet often as the activities was to be done on large scale
basis. The primary goal would be reaching schools and colleges of Mumbai
and migrate them to free software.  

There was a discussion on preparation of lab mauals, text books and other
open resources in digital form and distributing them to schools.
Krishnakant agreed to offer help in accounts and CAD content.  

Raghu informed everyone about Rushabh's proposal of organizing a workshop
for teachers on Software Freedom Day (16th Sept 2017) and take this
opportunity to inform about GNULinux and FOSS. Also make them aware about
the Migration, why it was important and how the four organizations unitedly
willing to help them as a social mission.  

Everyone supported the proposal. Raghu agreed to make poster and the
content would be provided by Siddharth. A quick distribution of other work
would be done on separate group made for this workshop where the volunteers
would be added. The workshop would be from 2 pm to 5 pm in DBIT and before
that a meeting of volunteers would take place.  

The discussion ended at about 7 pm.
-- End Milind's Post  

</div>

[View original post on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20170904/075070.html).  
[View original summary on the mailing list](http://mm.ilug-bom.org.in/pipermail/linuxers/Week-of-Mon-20170911/075072.html).  
