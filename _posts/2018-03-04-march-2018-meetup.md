---
title: March 2018 meetup
date: 2018-03-04 09:16:01 +0530
desc: March 2018 meetup at Don Bosco Institute of Technology.
category: event
---

The March 2018 meeting of "Indian Linux User Group, Mumbai (ILUG-BOM)",
will be held on 10th March 2018, 15:00 to 16:00 IST.

**Venue**:

IT Lab 7, First Floor, D - Wing,
Department of Information Technology,
Don Bosco Institute of Technology, Kurla (W).
10 min from Vidyavihar Station (W).
<a href="https://goo.gl/maps/fRQoetu97Vp" target="_blank">View Map</a>

**Agenda:**
1. Yet to be declared
