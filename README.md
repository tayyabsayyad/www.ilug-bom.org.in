# The ILUG-BOM Website

The GNU/Linux Users Group of Mumbai, India, also called ILUG-BOM, is
an informal mailing list of over 1000 members from all over India, but
mostly from Mumbai. This website provides a landing page for new
comers who want to join the community and for regulars to keep track
of the events.

# Running

Be sure to have `ruby`, `bundler` and `git` installed.

Then using your favourite terminal, run:

```
$ git clone https://gitlab.com/ilug-bom/www.ilug-bom.org.in ilugbom
$ cd ilugbom
$ bundle install
$ bundle exec jekyll serve
```

Now open http://localhost:4000 in your browser

# Thanks
Thanks to [Jackal](https://github.com/clenemt/jackal) for the theme and a template to base this website on.

# License
Copyright (c) 2017 ILUG-BOM Website Maintainers - Released under the GNU GPL-3 License.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
